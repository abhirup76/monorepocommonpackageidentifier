# Use an official Ubuntu as a parent image
FROM ubuntu:20.04

# Set environment variables to non-interactive mode
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    build-essential \
    libssl-dev \
    libreadline-dev \
    zlib1g-dev \
    libffi-dev \
    && rm -rf /var/lib/apt/lists/*

# Install rbenv and ruby-build
RUN git clone https://github.com/rbenv/rbenv.git ~/.rbenv
RUN git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
RUN ~/.rbenv/plugins/ruby-build/install.sh

# Set up rbenv
ENV PATH="/root/.rbenv/bin:/root/.rbenv/shims:${PATH}"
RUN echo 'eval "$(rbenv init -)"' >> ~/.bashrc
RUN eval "$(rbenv init -)"

# Install Ruby 2.7.8
RUN rbenv install 2.7.8
RUN rbenv global 2.7.8
RUN rbenv rehash

# Install bundler
RUN gem install bundler -v 2.4.22

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install -y nodejs