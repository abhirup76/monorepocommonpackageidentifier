const { spawn } = require('child_process');
const axios = require('axios');
require('dotenv').config();
const {getProjectIdFromProjectPath} = require('../../utils/getProjectId');
const {findPackageManagerFiles} = require('../../utils/findPackageManagerFiles');

const updateScript = async ()=>{
    const projectPath = process.env.PROJECT_PATH
    const gitlab_access_token = process.env.GITLAB_ACCESS_TOKEN;
    const projectId = await getProjectIdFromProjectPath(projectPath,gitlab_access_token);
    if(!projectId){
        return;
    }
    // const mergeRequests = await getMergeRequests(projectId);

    const packageManagerFiles = await findPackageManagerFiles(projectId,process.env.GITLAB_ACCESS_TOKEN);
    packageManagerFiles.forEach(packageManagerFile => {
        
        // setting the required environment variables

        const packageManager = packageManagerFile.packageManger;
        const directory = packageManagerFile.filePath;
        process.env.PACKAGE_MANAGER = packageManager
        process.env.DIRECTORY_PATH = directory

        //creating the ignore dependancies array based on different package manager and directory
        let ignoreDependancies = [];
        // mergeRequests.forEach(mergeRequest => {
        //     if(!mergeRequest.merged_by){
        //         const dependancy = getDependancyFromBranchName(mergeRequest,packageManager,directory);
        //         if(dependancy)
        //             ignoreDependancies.push(dependancy);
        //     }
        // });

        // executing the ruby wrapper script
        const rubyScriptPath = 'bots/dependabot/index.rb';
        const rubyProcess = spawn('ruby', [rubyScriptPath, JSON.stringify(ignoreDependancies)]);

        rubyProcess.stdout.on('data', (data) => {
            console.log(data.toString());
        });

        rubyProcess.stderr.on('data', (data) => {
            console.log(data.toString());
        });

        rubyProcess.on('close', (code) => {
            console.log(`Process exited with code ${code}`);
        });
    })
    
}

const getDependancyFromBranchName = (mergeRequest,packageManager,directory)=>{
    const branchName = mergeRequest.source_branch;
    let branchPattern = `dependabot/${packageManager}${directory=="/" ? "" : directory}/`;
    if(branchName.startsWith(branchPattern))
    {
        const branchNameWithVersion = branchName.substring(branchPattern.length);
        const dependancy = branchNameWithVersion.substring(0,branchNameWithVersion.lastIndexOf('-'));
        if(mergeRequest.title.includes(dependancy))
            return dependancy;
    }
}

const getMergeRequests = async (projectId)=>{
    if(!projectId)
    {
        //handle error
        console.error('Project details could not be fetched, please check your access tokens');
    }
    let page = 1;
    const perPage = 100;
    let allMergeRequests = [];
    let fetchMore = true;
    const daysToCheckAfter = 100; //Controls merge requests from how many days before to ignore
    while (fetchMore) {
        try {
            const mergeRequestResponse = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/merge_requests`, {
            headers: {
                'PRIVATE-TOKEN': gitlab_access_token
            },
            params: {
                per_page: perPage,
                page: page,
                search : 'Bump',
                in : 'title',
                scope : 'created_by_me',
                created_after: `${getISODateDaysBefore(daysToCheckAfter)}`
            }
            });
            if(! mergeRequestResponse || mergeRequestResponse.status!=200)
            {
                //handle error
                console.error('Failed to fetch gitlab apis');
                return;
            }
            const mergeRequests = await mergeRequestResponse.data;
            allMergeRequests = allMergeRequests.concat(mergeRequests);

            if (mergeRequests.length < perPage) {
            fetchMore = false;
            } else {
            page += 1;
            }
        } catch (error) {
            console.error('Error fetching merge requests:', error.response ? error.response.data : error.message);
            fetchMore = false;
        }
    }
    return allMergeRequests;
}

const getISODateDaysBefore = (days)=>{
    const today = new Date();
    today.setDate(today.getDate() - days);
    return today.toISOString();
}

updateScript();