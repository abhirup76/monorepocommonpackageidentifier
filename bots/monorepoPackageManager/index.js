//Entry point script that finds all the package manager files in repo and passes it to the ruby script index.rb

require('dotenv').config();
const {findPackageManagerFiles} = require('../../utils/findPackageManagerFiles')
const {getProjectIdFromProjectPath} = require('../../utils/getProjectId');
const {spawn} = require('child_process');

const main = async () => {
  if(!process.env.PROJECT_PATH)
  {
    console.error('Please set the PROJECT_PATH in environment variables');
    return;
  }
  if(!process.env.GITLAB_ACCESS_TOKEN)
  {
    console.error('Please set the GITLAB_ACCESS_TOKEN in environment variables')
    return;
  }
  const projectId = await getProjectIdFromProjectPath(process.env.PROJECT_PATH, process.env.GITLAB_ACCESS_TOKEN);

  if (!projectId) {
    console.error('Failed to fetch gitlab apis');
    return;
  }

  try {
    const packageManagerFiles = await findPackageManagerFiles(projectId,process.env.GITLAB_ACCESS_TOKEN);
    const rubyScriptPath = 'bots/monorepoPackageManager/index.rb';
    const rubyProcess = spawn('ruby', [rubyScriptPath, JSON.stringify(packageManagerFiles)]);

    rubyProcess.stdout.on('data', (data) => {
        console.log(data.toString());
    });

    rubyProcess.stderr.on('data', (data) => {
        console.log(data.toString());
    });

    rubyProcess.on('close', (code) => {
        console.log(`Process exited with code ${code}`);
    });
    
  } catch (error) {
    console.error('Error:', error.message);
  }
};

main();