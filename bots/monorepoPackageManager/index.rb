require "dependabot/file_fetchers"
require "dependabot/file_parsers"
require "dependabot/update_checkers"
require "dependabot/file_updaters"
require "dependabot/pull_request_creator"
require "dependabot/omnibus"
require "gitlab"
require "json"
require "net/http"
require "uri"


packageManagerFiles = JSON.parse(ARGV[0])

gitlab_hostname = ENV["GITLAB_HOSTNAME"] || "gitlab.com"

credentials = [
  {
    "type" => "git_source",
    "host" => "github.com",
    "username" => "x-access-token",
    "password" => ENV["GITHUB_ACCESS_TOKEN"] # A GitHub access token with read access to public repos
  },
  {
    "type" => "git_source",
    "host" => gitlab_hostname,
    "username" => "x-access-token",
    "password" => ENV["GITLAB_ACCESS_TOKEN"] # A GitLab access token with API permission
  }
]

repo_name = ENV["PROJECT_PATH"]
directory = ENV["DIRECTORY_PATH"]
branch = ENV["BRANCH"]

dependenciesVersionsHash = Hash.new
dependenciesLocationsHash = Hash.new

#Function to return minimum of two versions
def min_version(version1, version2)
  if !version1 or !version2
    return (version1 or version2)
  end
  if version1.major < version2.major
    return version1
  elsif version1.major > version2.major
    return version2
  end
  if version1.minor < version2.minor
    return version1
  elsif version1.minor > version2.minor
    return version2
  end
  if version1.patch < version2.patch
    return version1
  elsif version1.patch > version2.patch
    return version2
  end
  version1
end


#First loop to identify packages across repositories and their mutual common versions
packageManagerFiles.each do |packageManagerFile|
  directory = packageManagerFile['filePath']
  package_manager = packageManagerFile['packageManger']
  source = Dependabot::Source.new(
    provider: "gitlab",
    hostname: gitlab_hostname,
    api_endpoint: "https://#{gitlab_hostname}/api/v4",
    repo: repo_name,
    directory: directory,
    branch: nil
  )


  fetcher = Dependabot::FileFetchers.for_package_manager(package_manager).new(
    source: source,
    credentials: credentials
  )

  files = fetcher.files
  commit = fetcher.commit

  parser = Dependabot::FileParsers.for_package_manager(package_manager).new(
    dependency_files: files,
    source: source,
    credentials: credentials
  )

  dependencies = parser.parse

  dependencies.select(&:top_level?).each do |dep|

    dependencyName = dep.name

    if !dependenciesLocationsHash.key?(dependencyName)
      dependenciesLocationsHash[dependencyName]=[]
    end

    dependenciesLocationsHash[dependencyName].push({
      version: dep.version,
      location: directory
    })

    checker = Dependabot::UpdateCheckers.for_package_manager(package_manager).new(
      dependency: dep,
      dependency_files: files,
      credentials: credentials
    )

    version = checker.latest_resolvable_version_with_no_unlock
    if(!version)
      requirements_to_unlock =
        if !checker.requirements_unlocked_or_can_be?
          if checker.can_update?(requirements_to_unlock: :none) then :none
          else :update_not_possible
          end
        elsif checker.can_update?(requirements_to_unlock: :own) then :own
        elsif checker.can_update?(requirements_to_unlock: :all) then :all
        else :update_not_possible
        end

        next if requirements_to_unlock == :update_not_possible


        updated_deps = checker.updated_dependencies(
          requirements_to_unlock: requirements_to_unlock
        )
        version = updated_deps[0].latest_version
      end
    if dependenciesVersionsHash.key?(dependencyName)
      dependenciesVersionsHash[dependencyName] = min_version(dependenciesVersionsHash[dependencyName],version)
    else
      dependenciesVersionsHash[dependencyName] = version
    end
  end
end


#Send messages to the specified MS Teams webhook
dependenciesLocationsHash.each do |dependencyName, versionLocationArray|
  locations = ""
  version=dependenciesVersionsHash[dependencyName]
  versionLocationArray.each do |element|
    next if dependenciesVersionsHash[dependencyName]==element[:version]
    locations += (element[:location] + " ")
  end
  next if locations==""

  url = ENV["TEAMS_URL"]
  headers = {
    "Content-type": "application/json"
  }
  data = {
    "Summary": "Dependency update",
    "Text": "Update possible for #{repo_name}",
    "sections": [
        {
            "facts": [
                {
                  "name": "Project",
                  "value":"#{repo_name}"
                },
                {
                  "name": "Dependency name",
                  "value":"#{dependencyName}"
                },
                {
                  "name": "Upgrade to",
                  "value": "#{version}"
                },
                {
                  "name": "Locations of package manager file",
                  "value": "#{locations}"
                }
            ]
        }
    ]
  }
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = (uri.scheme == "https")

  request = Net::HTTP::Post.new(uri.request_uri, headers)
  request.body = data.to_json

  response = http.request(request)
end
