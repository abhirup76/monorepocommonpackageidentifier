const axios = require('axios');

const getProjectIdFromProjectPath = async (projectPath, gitlab_access_token)=>{
    if(!projectPath){
        console.error("Please set the project path");
        return;
    }
    if(!gitlab_access_token){
        console.error("Please set the gitlab access token");
        return;
    }
    const HEADERS = {
        'PRIVATE-TOKEN': gitlab_access_token
    };
    const projectDetailurl = `https://gitlab.com/api/v4/projects/${encodeURIComponent(projectPath)}`;
    const projectDetailsResponse = await axios.get(projectDetailurl, {headers : HEADERS});
    if(!projectDetailsResponse || projectDetailsResponse.status!=200)
    {
        //handle error
        console.error('Failed to fetch gitlab apis')
        return;
    }
    const data = await projectDetailsResponse.data;
    return data.id
}

module.exports={getProjectIdFromProjectPath};