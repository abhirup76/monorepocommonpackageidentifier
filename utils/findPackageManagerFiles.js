const axios = require('axios');

const packageManagerFiles = [
    'package.json', //Javascript
    'yarn.lock', //Yarn
    'requirements.txt', //Python
    'Gemfile', //Ruby
    'terraform.lock.hcl', //Terraform
    'mix.exs', //Elixir
    'composer.json', //PHP
    'go.mod' //Go
];

const packageManagers = [
    'npm_and_yarn',
    'npm_and_yarn',
    'pip',
    'bundler',
    'terraform',
    'hex',
    'composer',
    'go_modules'
]


const findPackageManagerFiles = async (projectId, gitlab_access_token, path = '', results = []) => {
    if(!gitlab_access_token){
        console.error("Please set the gitlab access token");
        return;
    }
    const HEADERS = {
        'PRIVATE-TOKEN': gitlab_access_token
    };
    const response = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/repository/tree`, {
        params: {
            path: path,
            per_page: 100
        },
        headers: HEADERS
    });

    const files = response.data;

    for (const file of files) {
        if (file.type === 'tree') {
            await findPackageManagerFiles(projectId, gitlab_access_token,file.path, results);
        } else {
            if (packageManagerFiles.includes(file.name)) {
                results.push({
                    filePath: ("/"+file.path.substring(0, file.path.length - file.name.length - 1)),
                    packageManger: packageManagers[packageManagerFiles.indexOf(file.name)]
                });
            }
        }
    }
    return results;
};

module.exports = { findPackageManagerFiles }